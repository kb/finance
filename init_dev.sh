#!/bin/bash
# exit immediately if a command exits with a nonzero exit status.
set -e
# treat unset variables as an error when substituting.
set -u

DB_NAME="dev_test_finance_`id -nu`"

psql -X -U postgres -c "DROP DATABASE IF EXISTS ${DB_NAME};"
psql -X -U postgres -c "CREATE DATABASE ${DB_NAME} TEMPLATE=template0 ENCODING='utf-8';"

django-admin.py migrate --noinput
django-admin.py demo_data_login
django-admin.py demo_data_finance
django-admin init_finance_app_european_union_countries
django-admin.py runserver
