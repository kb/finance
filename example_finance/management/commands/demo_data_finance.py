# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from finance.models import Country, VatSettings


class Command(BaseCommand):

    help = "Demo data for the finance app..."

    def handle(self, *args, **options):
        count = 0
        self.stdout.write("{}".format(self.help))
        # European Union Countries
        for name, iso2_code in VatSettings.EU_COUNTRIES.items():
            Country.objects.init_country(iso2_code, iso2_code, name)
            count = count + 1
        # VAT settings
        vat_settings = VatSettings.load()
        vat_settings.save()
        self.stdout.write("{} ({} records) - Complete".format(self.help, count))
