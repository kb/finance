# -*- encoding: utf-8 -*-
from braces.views import LoginRequiredMixin, StaffuserRequiredMixin
from django.views.generic import TemplateView

from base.view_utils import BaseMixin
from finance.models import VatSettings


class DashView(
    BaseMixin, LoginRequiredMixin, StaffuserRequiredMixin, TemplateView
):
    template_name = "example/dash.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        vat_settings = VatSettings.load()
        context.update({"vat_settings": vat_settings})
        return context


class HomeView(BaseMixin, TemplateView):
    template_name = "example/home.html"


class SettingsView(
    BaseMixin, LoginRequiredMixin, StaffuserRequiredMixin, TemplateView
):
    template_name = "example/settings.html"
