# -*- encoding: utf-8 -*-
from django.urls import re_path

from .views import VatSettingsUpdateView

urlpatterns = [
    re_path(
        r"^settings/update/$",
        view=VatSettingsUpdateView.as_view(),
        name="finance.vat.settings.update",
    )
]
