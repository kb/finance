# -*- encoding: utf-8 -*-
from braces.views import LoginRequiredMixin, SuperuserRequiredMixin
from django.urls import reverse
from django.views.generic import UpdateView

from base.view_utils import BaseMixin
from finance.forms import VatSettingsForm
from finance.models import VatSettings


class VatSettingsUpdateView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, UpdateView
):

    form_class = VatSettingsForm

    def get_object(self):
        return VatSettings.load()

    def get_success_url(self):
        return reverse("project.settings")
