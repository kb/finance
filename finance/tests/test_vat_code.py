# -*- encoding: utf-8 -*-
import pytest

from decimal import Decimal
from django.db import IntegrityError

from finance.models import VatCode
from finance.tests.factories import VatCodeFactory


@pytest.mark.django_db
def test_ordering():
    assert ["E", "L", "S", "Z", "ZEU"] == [
        x.slug for x in VatCode.objects.all()
    ]


@pytest.mark.parametrize(
    "code,description,rate,expect",
    [
        ("Q", "Legacy", Decimal("0.175"), "Legacy ('Q') 17.5%"),
        ("R", "", Decimal("0.20"), "R 20%"),
        ("R", "Standard", Decimal("0.20"), "Standard ('R') 20%"),
    ],
)
@pytest.mark.django_db
def test_str(code, description, rate, expect):
    vat_code = VatCodeFactory(slug=code, description=description, rate=rate)
    assert expect == str(vat_code)


@pytest.mark.parametrize(
    "code,expect",
    [
        ("L", "Legacy ('L') 0%"),
        ("S", "Standard ('S') 20%"),
        ("Z", "Zero-Rated (outside the EC) ('Z') 0%"),
    ],
)
@pytest.mark.django_db
def test_str_default(code, expect):
    vat_code = VatCode.objects.get(slug=code)
    assert expect == str(vat_code)


@pytest.mark.django_db
def test_vat_code():
    """Created in the migrations."""
    VatCode.objects.get(slug=VatCode.STANDARD)


@pytest.mark.django_db
def test_vat_code_exempt():
    vat_code = VatCode.objects.exempt()
    assert Decimal() == vat_code.rate


@pytest.mark.django_db
def test_vat_code_unique():
    """Check we can't create another standard VAT rate record."""
    obj = VatCode(slug=VatCode.STANDARD, rate=Decimal("20"))
    with pytest.raises(IntegrityError):
        obj.save()


@pytest.mark.django_db
def test_zero_rate_ec_vat_code():
    vat_code = VatCode.objects.zero_rate_ec_vat_code()
    assert VatCode.ZERO_RATE_EC == vat_code.slug
    assert Decimal() == vat_code.rate
