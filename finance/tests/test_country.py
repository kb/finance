# -*- encoding: utf-8 -*-
import pytest

from django.db import IntegrityError

from finance.models import Country
from .factories import CountryFactory


@pytest.mark.django_db
def test_init_country():
    CountryFactory(iso2_code="AD", iso3_code="AND", name="Andorra")
    x = Country.objects.init_country("AD", "ANR", "Andorra2")
    assert "AD" == x.iso2_code
    assert "ANR" == x.iso3_code
    assert "Andorra2" == x.name


@pytest.mark.django_db
def test_init_country_create():
    x = Country.objects.init_country("AD", "AND", "Andorra")
    assert "AD" == x.iso2_code
    assert "AND" == x.iso3_code
    assert "Andorra" == x.name


@pytest.mark.django_db
def test_ordering():
    CountryFactory(iso2_code="CC")
    CountryFactory(iso2_code="AA")
    CountryFactory(iso2_code="BB")
    assert ["AA", "BB", "CC"] == [x.iso2_code for x in Country.objects.all()]


@pytest.mark.django_db
def test_str():
    x = CountryFactory(iso2_code="AD", iso3_code="AND", name="Andorra")
    assert "AD Andorra (ISO3 'AND')" == str(x)


@pytest.mark.django_db
def test_unique_together():
    CountryFactory(iso2_code="AA")
    with pytest.raises(IntegrityError) as e:
        CountryFactory(iso2_code="AA")
    assert "duplicate key value violates unique constraint" in str(e.value)
