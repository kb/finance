# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse

from finance.tests.factories import VatSettingsFactory
from login.tests.fixture import perm_check


@pytest.mark.django_db
def test_vat_settings_update(perm_check):
    VatSettingsFactory()
    url = reverse("finance.vat.settings.update")
    perm_check.admin(url)
