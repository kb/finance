# -*- encoding: utf-8 -*-
import pytest

from django.core.management import call_command

from finance.models import FinanceError
from finance.tests.factories import VatSettingsFactory


@pytest.mark.django_db
def test_init_finance_app_european_union_countries():
    VatSettingsFactory()
    with pytest.raises(FinanceError) as e:
        call_command("init_finance_app_european_union_countries")
    assert "Cannot find European Union Country with ISO alpha-2 code" in str(
        e.value
    )
