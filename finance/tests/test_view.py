# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus

from finance.models import VatCode, VatSettings
from finance.tests.factories import CountryFactory
from login.tests.factories import TEST_PASSWORD, UserFactory


@pytest.mark.django_db
def test_vat_settings_update(client):
    # login
    user = UserFactory(is_staff=True, is_superuser=True)
    vat_code_eu = VatCode.objects.zero_rate_ec_vat_code()
    vat_code_standard = VatCode.objects.get(slug=VatCode.EXEMPT)
    CountryFactory(iso2_code="AA")
    country_bb = CountryFactory(iso2_code="BB")
    CountryFactory(iso2_code="CC")
    country_dd = CountryFactory(iso2_code="DD")
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("finance.vat.settings.update")
    response = client.post(
        url,
        data={
            "standard_vat_code": vat_code_standard.pk,
            "zero_rate_ec_vat_code": vat_code_eu.pk,
            "vat_number": "ABC",
            "european_union_countries": [country_bb.pk, country_dd.pk],
        },
    )
    # check
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("project.settings") == response.url
    assert 1 == VatSettings.objects.count()
    vat_settings = VatSettings.load()
    assert set(["BB", "DD"]) == {
        x.iso2_code for x in vat_settings.european_union_countries.all()
    }
