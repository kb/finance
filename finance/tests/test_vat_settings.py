# -*- encoding: utf-8 -*-
import pytest

from decimal import Decimal

from finance.models import Country, FinanceError, VatSettings
from finance.tests.factories import CountryFactory, VatSettingsFactory


@pytest.mark.django_db
def test_european_union_countries():
    vat_settings = VatSettings.load()
    vat_settings.save()
    CountryFactory(iso2_code="AA")
    bb = CountryFactory(iso2_code="BB")
    CountryFactory(iso2_code="CC")
    dd = CountryFactory(iso2_code="DD")
    vat_settings.european_union_countries.add(bb, dd)
    assert set(["BB", "DD"]) == {
        x.iso2_code for x in vat_settings.european_union_countries.all()
    }


@pytest.mark.django_db
def test_european_union_countries_initialised():
    country = CountryFactory(iso2_code="AA")
    vat_settings = VatSettings.load()
    vat_settings.save()
    vat_settings.european_union_countries.add(country)
    assert VatSettings.objects.european_union_countries_initialised() is True


@pytest.mark.django_db
def test_european_union_countries_initialised_not_init():
    vat_settings = VatSettings.load()
    vat_settings.save()
    with pytest.raises(FinanceError) as e:
        VatSettings.objects.european_union_countries_initialised()
    assert (
        "There are no European Union Countries in the database "
        "(did you run the 'magento_sync_settings' management command)?"
    ) in str(e.value)


@pytest.mark.django_db
def test_european_union_countries_initialised_not_saved():
    with pytest.raises(FinanceError) as e:
        VatSettings.objects.european_union_countries_initialised()
    assert "VAT settings have not been setup" in str(e.value)


@pytest.mark.django_db
def test_init_european_union_countries():
    vat_settings = VatSettings.load()
    vat_settings.save()
    for k, iso2_code in VatSettings.EU_COUNTRIES.items():
        CountryFactory(iso2_code=iso2_code)
    for iso2_code in ("AA", "BB", "CC", "DD"):
        CountryFactory(iso2_code=iso2_code)
    assert 29 == VatSettings.objects.init_european_union_countries()
    # check
    for k, iso2_code in VatSettings.EU_COUNTRIES.items():
        country = Country.objects.get(iso2_code=iso2_code)
        assert VatSettings.objects.is_european_union_country(country) is True
    for iso2_code in ("AA", "BB", "CC", "DD"):
        country = Country.objects.get(iso2_code=iso2_code)
        assert VatSettings.objects.is_european_union_country(country) is False


@pytest.mark.django_db
def test_init_european_union_countries_missing():
    vat_settings = VatSettings.load()
    vat_settings.save()
    for iso2_code in ("AT", "BE", "BG"):
        CountryFactory(iso2_code=iso2_code)
    with pytest.raises(FinanceError) as e:
        VatSettings.objects.init_european_union_countries()
    assert "Cannot find European Union Country with ISO alpha-2 code" in str(
        e.value
    )


@pytest.mark.django_db
def test_is_european_union_country():
    vat_settings = VatSettings.load()
    vat_settings.save()
    aa = CountryFactory(iso2_code="AA")
    bb = CountryFactory(iso2_code="BB")
    cc = CountryFactory(iso2_code="CC")
    dd = CountryFactory(iso2_code="DD")
    vat_settings.european_union_countries.add(bb, dd)
    assert VatSettings.objects.is_european_union_country(aa) is False
    assert VatSettings.objects.is_european_union_country(bb) is True
    assert VatSettings.objects.is_european_union_country(cc) is False
    assert VatSettings.objects.is_european_union_country(dd) is True


@pytest.mark.django_db
def test_vat_settings_str():
    vat_settings = VatSettingsFactory()
    str(vat_settings)


@pytest.mark.django_db
def test_vat_settings_manager():
    VatSettingsFactory()
    vat_settings = VatSettings.objects.settings()
    assert Decimal("0.20") == vat_settings.standard_vat_code.rate


@pytest.mark.django_db
def test_vat_settings_manager_not():
    with pytest.raises(FinanceError) as e:
        VatSettings.objects.settings()
    assert "VAT settings have not been setup" in str(e.value)


# @pytest.mark.django_db
# def test_zero_rate_ec_vat_code():
#    vat_settings = VatSettings.load()
#    assert "ZEU" == vat_settings.zero_rate_ec_vat_code
