# -*- encoding: utf-8 -*-
import pytest

from finance.models import default_vat_code, legacy_vat_code, VatCode


@pytest.mark.django_db
def test_default_vat_code():
    vat_code = VatCode.objects.get(slug=VatCode.STANDARD)
    assert vat_code.pk == default_vat_code()


@pytest.mark.django_db
def test_legacy_vat_code():
    vat_code = VatCode.objects.get(slug=VatCode.LEGACY)
    assert vat_code.pk == legacy_vat_code()
