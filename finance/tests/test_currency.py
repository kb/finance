# -*- encoding: utf-8 -*-
import pytest

from finance.models import Currency


@pytest.mark.parametrize(
    "slug,description",
    [
        (Currency.DOLLAR, "United States Dollar"),
        (Currency.EURO, "Euro"),
        (Currency.POUND, "Pound Sterling"),
    ],
)
@pytest.mark.django_db
def test_exists(slug, description):
    """We create the Euro, Dollar and Pound in a migration."""
    currency = Currency.objects.get(slug=slug)
    assert description == currency.description


@pytest.mark.django_db
def test_ordering():
    assert ["EUR", "GBP", "USD"] == [x.slug for x in Currency.objects.all()]


@pytest.mark.django_db
def test_str():
    currency = Currency.objects.get(slug="GBP")
    assert "GBP - Pound Sterling" == str(currency)
