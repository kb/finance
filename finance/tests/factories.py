# -*- encoding: utf-8 -*-
import factory

from finance.models import Country, Currency, VatCode, VatSettings


class CountryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Country

    @factory.sequence
    def name(n):
        return "name_{}".format(n)


class CurrencyFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Currency

    @factory.sequence
    def slug(n):
        return "slug_{}".format(n)


class VatSettingsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = VatSettings

    @factory.lazy_attribute
    def standard_vat_code(self):
        return VatCode.objects.get(slug=VatCode.STANDARD)


class VatCodeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = VatCode
