# -*- encoding: utf-8 -*-
from decimal import Decimal
from django.db import models
from reversion import revisions as reversion

from base.model_utils import TimeStampedModel
from base.singleton import SingletonModel


def default_vat_code():
    return VatCode.objects.get(slug=VatCode.STANDARD).pk


def legacy_vat_code():
    """For records which were created before we introduced VAT codes."""
    return VatCode.objects.get(slug=VatCode.LEGACY).pk


class FinanceError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("%s, %s" % (self.__class__.__name__, self.value))


class CountryManager(models.Manager):
    def _create_country(self, iso2_code, iso3_code, name):
        x = self.model(iso2_code=iso2_code, iso3_code=iso3_code, name=name)
        x.save()
        return x

    def init_country(self, iso2_code, iso3_code, name):
        try:
            x = self.model.objects.get(iso2_code=iso2_code)
            x.iso2_code = iso2_code
            x.iso3_code = iso3_code
            x.name = name
            x.save()
        except self.model.DoesNotExist:
            x = self._create_country(iso2_code, iso3_code, name)
        return x


class Country(models.Model):
    """Countries.

    https://devdocs.magento.com/guides/m1x/api/soap/directory/directory_country.list.html

    """

    iso2_code = models.CharField(max_length=2, unique=True)
    iso3_code = models.CharField(max_length=3)
    name = models.CharField(max_length=100)
    objects = CountryManager()

    class Meta:
        ordering = ("iso2_code",)
        verbose_name = "Countries"

    def __str__(self):
        return "{} {} (ISO3 '{}')".format(
            self.iso2_code, self.name, self.iso3_code
        )


class VatCodeManager(models.Manager):
    def exempt(self):
        return self.model.objects.get(slug=self.model.EXEMPT)

    def zero_rate_ec_vat_code(self):
        return self.model.objects.get(slug=self.model.ZERO_RATE_EC)


class VatCode(TimeStampedModel):
    """VAT code and rates.

    https://www.gov.uk/rates-of-vat-on-different-goods-and-services

    VAT rates for goods and services
    Rate            % of VAT    What the rate applies to
    Standard        20%         Most goods and services
    Reduced rate    5%          Some goods and services, eg home energy
    Zero rate       0%          Zero-rated goods and services, eg most food

    https://www.gov.uk/rates-of-vat-on-different-goods-and-services

    No VAT is charged on goods or services that are:

    - exempt from VAT
    - outside the scope of the UK VAT system

    """

    EXEMPT = "E"
    FIFTEEN = "15"
    LEGACY = "L"
    SEVENTEEN_POINT_FIVE = "17-5"
    STANDARD = "S"
    ZERO_RATE = "Z"
    ZERO_RATE_EC = "ZEU"

    slug = models.SlugField(max_length=10, unique=True)
    description = models.CharField(max_length=100)
    rate = models.DecimalField(
        max_digits=5,
        decimal_places=3,
        help_text="For a 20% VAT rate, enter '0.200'",
    )
    deleted = models.BooleanField(default=False)
    objects = VatCodeManager()

    class Meta:
        verbose_name = "VAT code"

    def __str__(self):
        rate = str(self.rate * Decimal("100")).rstrip("0").rstrip(".")
        if self.description:
            return "{} ('{}') {}%".format(self.description, self.slug, rate)
        else:
            return "{} {}%".format(self.slug, rate)


reversion.register(VatCode)


class Currency(TimeStampedModel):
    """Currency code.

    For the slug we use ISO 4217:
    https://en.wikipedia.org/wiki/ISO_4217

    - EUR Euro
    - GBP Pound sterling
    - USD United States dollar

    These currency codes are created in::

      finance/migrations/0006_auto_20190226_1200.py

    """

    EURO = "EUR"
    POUND = "GBP"
    DOLLAR = "USD"

    slug = models.SlugField(max_length=10, unique=True)
    description = models.CharField(max_length=100)
    icon = models.CharField(max_length=50)
    deleted = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Currency"

    def __str__(self):
        return "{} - {}".format(self.slug, self.description)


reversion.register(Currency)


class VatSettingsManager(models.Manager):
    def european_union_countries_initialised(self):
        """Check the European Union Countries have been initialised."""
        vat_settings = self.settings()
        result = bool(vat_settings.european_union_countries.count())
        if not result:
            raise FinanceError(
                "There are no European Union Countries in the database (did "
                "you run the 'magento_sync_settings' management command)?"
            )
        return result

    def init_european_union_countries(self):
        count = 0
        vat_settings = self.settings()
        for name, iso2_code in self.model.EU_COUNTRIES.items():
            try:
                country = Country.objects.get(iso2_code=iso2_code)
                vat_settings.european_union_countries.add(country)
            except Country.DoesNotExist:
                raise FinanceError(
                    "Cannot find European Union Country with "
                    "ISO alpha-2 code '{}' ({})".format(iso2_code, name)
                )
            count = count + 1
        return count

    def is_european_union_country(self, country):
        return self.model.objects.filter(
            european_union_countries=country
        ).exists()

    def settings(self):
        try:
            return self.model.objects.get()
        except self.model.DoesNotExist:
            raise FinanceError("VAT settings have not been setup")


class VatSettings(SingletonModel):
    EU_COUNTRIES = {
        "Austria": "AT",
        "Belgium": "BE",
        "Bulgaria": "BG",
        "Croatia": "HR",
        "Cyprus": "CY",
        "Czech Republic": "CZ",
        "Denmark": "DK",
        "Estonia": "EE",
        "Finland": "FI",
        "France": "FR",
        "Germany": "DE",
        "Greece": "GR",
        "Hungary": "HU",
        "Ireland": "IE",
        "Italy": "IT",
        "Latvia": "LV",
        "Lithuania": "LT",
        "Luxembourg": "LU",
        "Malta": "MT",
        "Netherlands": "NL",
        "Poland": "PL",
        "Portugal": "PT",
        "Romania": "RO",
        "San Marino": "SM",
        "Slovakia": "SK",
        "Slovenia": "SI",
        "Spain": "ES",
        "Sweden": "SE",
        # "United Kingdom": "GB",
        "Vatican City": "VA",
    }

    standard_vat_code = models.ForeignKey(
        VatCode,
        default=default_vat_code,
        related_name="+",
        on_delete=models.CASCADE,
    )
    zero_rate_vat_code = models.ForeignKey(
        VatCode,
        help_text="Zero-rated goods and services",
        blank=True,
        null=True,
        related_name="+",
        on_delete=models.CASCADE,
    )
    zero_rate_ec_vat_code = models.ForeignKey(
        VatCode,
        help_text="EU customers who are registered for VAT",
        blank=True,
        null=True,
        related_name="+",
        on_delete=models.CASCADE,
    )
    vat_number = models.CharField(max_length=12, blank=True)
    european_union_countries = models.ManyToManyField(
        Country,
        help_text="Countries which can use the 'Zero Rated EC' VAT code",
    )
    objects = VatSettingsManager()

    class Meta:
        verbose_name = "VAT settings"

    def __str__(self):
        return "Standard: {}, VAT Number: {}".format(
            self.standard_vat_code.rate, self.vat_number
        )


reversion.register(VatSettings)
