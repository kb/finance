# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from finance.models import VatSettings


class Command(BaseCommand):

    help = "Initialise Finance app (European Union Countries)"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        count = VatSettings.objects.init_european_union_countries()
        self.stdout.write("{} ({} records) - Complete".format(self.help, count))
