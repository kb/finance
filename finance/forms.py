# -*- encoding: utf-8 -*-
from django import forms

from finance.models import VatSettings


class VatSettingsForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name in (
            "european_union_countries",
            "standard_vat_code",
            "zero_rate_vat_code",
            "zero_rate_ec_vat_code",
        ):
            f = self.fields[name]
            f.required = False
            f.widget.attrs.update({"class": "chosen-select"})

    class Meta:
        model = VatSettings
        fields = (
            "standard_vat_code",
            "zero_rate_vat_code",
            "zero_rate_ec_vat_code",
            "vat_number",
            "european_union_countries",
        )
