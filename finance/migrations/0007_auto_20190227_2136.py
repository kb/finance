# Generated by Django 2.1.7 on 2019-02-27 21:26
from django.db import migrations


def _init_state(model, slug, description, icon):
    try:
        model.objects.get(slug=slug)
    except model.DoesNotExist:
        instance = model(**dict(slug=slug, description=description, icon=icon))
        instance.save()
        instance.full_clean()


def default_state(apps, schema_editor):
    model = apps.get_model("finance", "Currency")
    _init_state(model, "EUR", "Euro", "fa-eur")
    _init_state(model, "GBP", "Pound Sterling", "fa-gbp")
    _init_state(model, "USD", "United States Dollar", "fa-usd")


class Migration(migrations.Migration):
    dependencies = [("finance", "0006_currency_icon")]
    operations = [migrations.RunPython(default_state)]
